'use strict';

const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

const openingHours = {
  [weekdays[3]]: {
    open: 12,
    close: 22,
  },
  [weekdays[4]]: {
    open: 11,
    close: 23,
  },
  [weekdays[5]]: {
    open: 0, // Open 24 hours
    close: 24,
  },
};

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  // ES6 enhanced object literals
  openingHours,
  orderDelivery({ starterIndex = 1, mainIndex = 0, address, time = '20:00' }) {
    console.log(
      `Order received ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`
    );
  },
  orderPasta: function (ing1, ing2, ing3) {
    console.log(`Here is your delicious pasta with ${ing1}, ${ing2}, ${ing3} `);
  },
  orderPizza(mainIngredient, ...otherIngredients) {
    console.log(mainIngredient);
    console.log(otherIngredients);
  },
};

console.log(flights);
console.log(flights.split(`+`));

const getCode = str => str.slice(0, 3).toUpperCase();

for (const flight of flights.split(`+`)) {
  console.log(flight);
  console.log(flight.split(`;`));
  const [type, from, to, time] = flight.split(`;`);
  const output = `${type.startsWith(`_Delayed`) ? `🔴` : ``}${type.replaceAll(
    `_`,
    ` `
  )} from ${getCode(from)} to ${getCode(to)} (${time.replace(
    `:`,
    `h`
  )})`.padStart(36);
  console.log(output);
}

// console.log(`a+very+nice+string`.split(`+`));
// console.log(`Alzy Welzy`.split(` `));

// const [firstName, lastName] = `Alzy Welzy`.split(` `);

// console.log(firstName);
// console.log(lastName);
// console.log(firstName, lastName);

// const fullName = [`Mr.`, firstName, lastName.toUpperCase()].join(` `);

// console.log(fullName);

// const capitalizeName = function (name) {
//   const names = name.split(` `);
//   const namesUpper = [];

//   for (const n of names) {
//     // namesUpper.push(n[0].toUpperCase() + n.slice(1));
//     namesUpper.push(n.replace(n[0], n[0].toUpperCase()));
//     console.log(n);
//     console.log(namesUpper);
//   }
//   console.log(namesUpper);
// };

// capitalizeName(`Jessica ann smith davis`);
// capitalizeName(`alzy welzy`);

// const message = `Go to gate 23`;
// console.log(message.padStart(25, `+`));
// console.log(message.padStart(25, `+`).padEnd(`35`, `+`));
// console.log(`ehe`.padStart(25, `+`));
// console.log(`ehe`.padStart(25, `+`).padEnd(`35`, `+`));

// const maskCreditCard = function (number) {
//   const str = String(number);
//   const last = str.slice(-4);
//   return last.padStart(str.length, `*`);
// };

// console.log(maskCreditCard(234334542354354325234));
// console.log(maskCreditCard(`343423432424124343243432423432`));

// // Repeat
// const message2 = `Bad Weather... All Departures Delayed... \n`;
// console.log(message2.repeat(5000));

// const planesInLine = function (n) {
//   console.log(`There are ${n} planes in line ${`✈️`.repeat(8)}`);
// };

// planesInLine(19);

// const airline = `Tap Air Portugal`;

// console.log(airline.toLowerCase());
// console.log(airline.toUpperCase());

// const passenger = `AlZy WElzY`;
// const passengerLower = passenger.toLowerCase();
// const passengerUppercase = passenger.toUpperCase();
// const passengerCapital =
//   passenger[0].toUpperCase() +
//   passengerLower.slice(1, 5).toLowerCase() +
//   passenger[5].toUpperCase() +
//   passengerLower.slice(6).toLowerCase();

// console.log(passengerLower);
// console.log(passengerUppercase);
// console.log(passengerCapital);

// const email = `hello@alzywelzy.com`;
// const loginEmail = ` Hello@AlzyWelzy.com`;

// const lowerEmail = loginEmail.toLowerCase();

// const trimmedEmail = lowerEmail.trim();
// console.log(email);
// console.log(loginEmail);
// console.log(lowerEmail);
// console.log(trimmedEmail);

// const normalizedEmail = loginEmail.toLowerCase().trim();
// console.log(normalizedEmail);

// console.log(email === normalizedEmail);

// const priceIN = `288.97 INR`;
// const priceUSD = priceIN.replace(`INR`, `USD`);

// console.log(priceIN);
// console.log(priceUSD);

// const announcement = `All passengers come to boarding door 23. Boarding door 23!`;

// console.log(announcement);

// console.log(announcement.replace(`door`, `gate`));

// console.log(announcement);

// console.log(announcement.replaceAll(`door`, `gate`));

// console.log(announcement);

// const plane = `A320neo`;
// console.log(plane);
// console.log(plane.includes(`neo`));
// console.log(plane.includes(`Ehe`));
// console.log(plane.startsWith(`A3`));
// console.log(plane.startsWith(`Baby`));

// if (plane.startsWith(`Airbus`) && plane.endsWith(`neo`)) {
//   console.log(`Part of the new Airbus family`);
// } else {
//   console.log(`OOPS IT DOESN'T`);
// }

// // Correct Way
// const checkBaggage = function (items) {
//   const baggage = items.toLowerCase();
//   if (baggage.includes(`knife`) || baggage.includes(`gun`)) {
//     console.log(`YOU AREN'T ALLOWED HERE`);
//   } else {
//     console.log(`Welcome aboard`);
//   }
// };

// // Wrong Way
// const checkBaggageWrongWay = function (items) {
//   // const baggage = items.toLowerCase();
//   const baggage = items;
//   if (baggage.includes(`knife`) || baggage.includes(`gun`)) {
//     console.log(`YOU AREN'T ALLOWED HERE`);
//   } else {
//     console.log(`Welcome aboard`);
//   }
// };

// checkBaggage(`I have a laptop, some Food and a pocket Knife`);
// checkBaggage(`Socks and Camera`);
// checkBaggage(`Got some snacks and a gun for protection`);

// checkBaggageWrongWay(`I have a laptop, some Food and a pocket Knife`);
// checkBaggageWrongWay(`Socks and Camera`);
// checkBaggageWrongWay(`Got some snacks and a gun for protection`);

// const airline = `TAP AIR PORTUGAL`;
// const plane = `A320`;

// console.log(plane);
// console.log(plane[0]);
// console.log(plane[1]);

// for (let i of plane) {
//   console.log(i);
// }

// console.log(`B737`[0]);

// console.log(airline.length);
// console.log(`B737`.length);

// console.log(airline.indexOf(`r`)); // returns -1 which means it doesn't exist
// console.log(airline.indexOf(`R`)); // returns a positive integer which means it exists
// console.log(airline.lastIndexOf('R')); // returns a positive integer which means it exists

// console.log(airline.indexOf(`PORTUGAL`));

// console.log(airline.slice(4));

// const airSlice = airline.slice();
// console.log(airSlice);

// console.log(airSlice.slice(4, 7));

// const airSlice47 = airline.slice(4, 7);
// console.log(airSlice47);

// const airSliceGap = airline.slice(0, airline.indexOf(` `));
// console.log(airSliceGap);

// const airlineGapLast = airline.slice(0, airline.lastIndexOf(` `));
// console.log(airlineGapLast);

// const airlineNegLast = airline.slice(-2);
// console.log(airlineNegLast);

// console.log(airline.slice(1, -1));

// const checkMiddleSeat = function (seat) {
//   // B and E are the middle seats
//   const s = seat.slice(-1);
//   if (s === `B` || s === `E`) {
//     console.log(`You got the middle seat`);
//   } else {
//     console.log(`YOU GOT LUCKY MY BUD`);
//   }
//   console.log(s);
// };

// checkMiddleSeat(`11B`);
// checkMiddleSeat(`23C`);
// checkMiddleSeat(`3E`);

// console.log(new String(`AlzyWelzy`));
// console.log(typeof new String(`AlzyWelzy`));
// console.log(typeof new String(`AlzyWelzy`).slice(1));

// const question = new Map([
//   [
//     `question`,
//     `Which among them is the best Programming Language?\n1. C, \n2. Java, \n3. JavaScript`,
//   ],
//   [1, `C`],
//   [2, `Java`],
//   [3, `JavaScript`],
//   [`correct`, 3],
//   [true, `Correct Answer YOHOO!`],
//   [false, `Try Again!`],
// ]);

// console.log(question);
// console.log(...question.entries());

// const answer = Number(prompt(question.get('question')));
// console.log(question.get(answer === question.get('correct')));

// console.log([...question]);
// console.log(question.entries());
// console.log(question.keys());
// console.log(question.values());

// Convert Object into Map
// console.log(Object.entries(openingHours));
// const hoursMap = new Map(Object.entries(openingHours));

// console.log(hoursMap);

// for (const [key, value] of question) {
//   // console.log(key, value);
//   if (typeof key === 'number') console.log(`Answer ${key}: ${value}`);
// }

// const answer = Number(
//   prompt(
//     `Which among them is the best Programming Language?\n1. C, \n2. Java, \n3. JavaScript`
//   )
// );
// console.log(answer);

// if (answer === 3) {
//   console.log(`YUP THAT'S CORRECT!`);
// } else {
//   console.log(`THAT'S THE WRONG ANSWER`);
// }

// const rest = new Map();
// rest.set('name', 'Classico Italiano');
// rest.set(1, 'Firenze, Italy');
// rest.set(2, 'Lisbon, Portugal');

// console.log(rest);

// rest
//   .set('categories', ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'])
//   .set('open', 11)
//   .set('close', 23)
//   .set(true, 'We are open :D')
//   .set(false, 'We are closed :(');

// console.log(rest);

// console.log(rest.get('name'));
// console.log(rest.get(true));
// console.log(rest.get(1));

// const time = 21;
// console.log(rest.get(time > rest.get('open') && time < rest.get('close')));

// console.log(rest.has('categories'));
// console.log(rest);
// rest.delete(2);
// console.log(rest);
// console.log(rest.size);
// rest.clear();
// console.log(rest);

// const arr = [1, 2];
// // rest.set([1,2],"Test")
// rest.set(arr, 'Test');
// rest.set(document.querySelector('h1'), 'Heading 1');
// console.log(rest);
// console.log(rest.size);

// console.log(rest.get([1, 2]));
// console.log(rest.get(arr));
// const orderSet = new Set([
//   'Pasta',
//   'Pizza',
//   'Pizza',
//   'Risotto',
//   'Pasta',
//   'Pizza',
// ]);

// console.log(orderSet);

// console.log(new Set('Alzy'));

// console.log(orderSet.size);
// console.log(orderSet.has('Pizza'));
// console.log(orderSet.has('Bread'));
// console.log(orderSet);
// orderSet.add('Garlic Bread');
// orderSet.add('Garlic Bread');

// orderSet.delete('Risotto');
// // orderSet.clear()

// for (const order of orderSet) console.log(order);

// // Example
// const staff = ['Waiter', 'Chef', 'Waiter', 'Manager', 'Chef', 'Waiter'];
// console.log(staff);
// // const staffUnique = new Set(staff);
// const staffUnique = [...new Set(staff)];
// console.log(staffUnique);
// console.log(
//   new Set(['Waiter', 'Chef', 'Waiter', 'Manager', 'Chef', 'Waiter']).size
// );

// console.log(new Set('AlzyWelzy').size);

// Use any data structure, and then, using the new ES6 feature, loop over it, and print each property to the console

// HINT: Looping Arrays: The for-of Loop

// HINT 2: Looping Objects: Object Keys, Values, and Entries

//  use looping objects

// then print `we are open on ${day} at ${time.open} till ${time.close}`
// if the restaurant is closed, print `we are closed on ${day}`

// const properties = Object.keys(openingHours);
// console.log(properties);

// let openStr = `we are open on ${properties.length} days: `;
// for (const day of properties) {
//   openStr += `${day}, `;
// }
// console.log(openStr);

// // Property VALUES
// const values = Object.values(openingHours);
// console.log(values);

// // Entire object
// const entries = Object.entries(openingHours);
// console.log(entries);

// // [key, value]
// for (const [key, { open, close }] of entries) {
//   console.log(key, open, close);
//   console.log(`we are open on ${key} at ${open} till ${close}`);
// }

// const properties = Object.keys(openingHours);
// console.log(properties);
// const values = Object.values(openingHours);
// console.log(values);
// console.log(values[0].open);

// for (const time of Object.values(values)) {
//   console.log(time);
// }
// let i = 0;
// for (const day of properties) {
//   let openStr = `We are open on ${day} at ${values[i].open} till ${values[i].close}.`;
//   i++;
//   console.log(openStr);
// }

// let openStr = `We are open on ${properties.length} days `;

// for (const day of properties) {
//   if (day === `sat`) {
//     openStr += `${day}.`;
//     continue;
//   }
//   openStr += `${day}, `;
// }

// console.log(openStr);

// const values = Object.values(openingHours);
// console.log(values);
// console.log(values.length);
// console.log(Array.isArray(values));

// for (const time of values) {
//   console.log(
//     `We are open at ${time.open} and close at ${time.close} every thur-sat.`
//   );
// }

// const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];

// for (const item of menu) console.log(item);

// console.log(...menu.entries());

// for (const item of menu.entries()) {
//   console.log(`${item[0] + 1}: ${item[1]}`);
//   console.log(item);
// }

// for (const [i, el] of menu.entries()) {
//   console.log(`${i + 1}: ${el}`);
// }

// console.log(...menu.entries());

// const [i, el] = menu.entries();

// console.log(i, el);

// console.log(i);

// console.log(el);

// console.log(openingHours);

// console.log(restaurant.openingHours);

// if (restaurant.openingHours.mon) console.log(restaurant.openingHours.mon.open);
// if (restaurant.openingHours.fri) console.log(restaurant.openingHours.fri.open);

// // WITH optional chaining

// console.log(restaurant.openingHours.mon?.open);
// console.log(restaurant.openingHours?.mon?.open ? 'yes' : 'no');

// // Example
// const days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

// for (const day of days) {
//   const open = restaurant.openingHours[day]?.open ?? 'closed';
//   console.log(`On ${day}, we open at ${open}`);
// }

// // Methods
// console.log(restaurant.order?.(0, 1) ?? 'Method does not exist');
// console.log(restaurant.orderEHE?.(0, 1) ?? 'Method does not exist');

// // Arrays
// const users = [{ name: 'Alzy', email: `hello@Alzy.io` }];

// console.log(users[0]?.name ?? 'User array empty');

// if (users.length > 0) console.log(users[0].name);
// else console.log('User array empty');

// const rest1 = {
//   name: 'Capri',
//   // numGuests: 20,
//   numGuests: 0,
// };

// const rest2 = {
//   name: 'La Piazza',
//   owner: 'Giovanni Rossi',
// };

// rest1.numGuests = rest1.numGuests || 10;
// rest2.numGuests = rest2.numGuests || 10;

// console.log(rest1);
// console.log(rest2);

// rest1.numGuests ||= 10;
// rest2.numGuests ||= 10;
// rest1.numGuests ??= 10;
// rest2.numGuests ??= 10;

// rest1.owner = rest1.owner && '<Anonymous>';
// rest2.owner = rest2.owner && '<Anonymous>';

// rest1.owner &&= '<Anonymous>';
// rest2.owner &&= '<Anonymous>';

// console.log(rest1);
// console.log(rest2);

// const rest1 = {
//   name: 'Capri',
//   numGuests: 0,
// };

// const rest2 = {
//   name: 'La Piazza',
//   owner: 'Giovanni Rossi',
// };

// rest1.numGuests = rest1.numGuests || 10;
// rest2.numGuests = rest2.numGuests || 10;

// rest1.numGuests ||= 10;
// rest2.numGuests ||= 10;

// rest1.numGuests ??= 10;
// rest2.numGuests ??= 10;

// console.log(rest1.numGuests);
// console.log(rest2.numGuests);

// console.log(3 || 'Alzy');
// console.log('' || 'Alzy');
// console.log(true || 0);
// console.log(undefined || null);

// console.log(undefined || 0 || '' || 'Hello' || 23 || null);

// restaurant.numGuests = 23;
// const guests1 = restaurant.numGuests ? restaurant.numGuests : 10;
// console.log(guests1);

// const guests2 = restaurant.numGuests || 10;
// console.log(guests2);

// console.log('-----AND-----');
// console.log(0 && 'Alzy');
// console.log(7 && 'Alzy');

// console.log('Hello' && 23 && null && 'Alzy');

// restaurant.numGuests = 0;
// const guests1 = restaurant.numGuests ? restaurant.numGuests : 10;
// console.log(guests1);

// const guests2 = restaurant.numGuests || 10;
// console.log(guests2);

// console.log('-----AND-----');
// console.log(0 && 'Alzy');
// console.log(7 && 'Alzy');

// console.log('Hello' && 23 && null && 'Alzy');

// const arr = [2, 3, 4, 5];

// const a = arr[0];
// const b = arr[1];
// const c = arr[2];
// const d = arr[3];

// let [w, x, y, z] = arr;

// console.log(w, x, y, z);
// console.log(arr);
// console.log(...arr);
// console.log(a, b, c, d);

// console.log(typeof arr, typeof x);

// const temp = x;
// x = y;
// y = temp;

// console.log(typeof arr, typeof x, typeof temp, typeof y, temp);

// console.log(restaurant.order(2, 0), typeof restaurant.order(2, 0));

// const nested = [2, 4, [5, 6]];

// const [p, q, [r, s]] = nested;

// const [p = 1, q = 1, [r = 1, s = 1]] = [1, 2];
// Default values
// console.log(p, q, r, s, typeof nested, typeof r);

/////////////////////////////////////////////////

// const { name: restaurantName, openingHours, categories } = restaurant;
// // console.log(name, openingHours, categories);
// console.log(restaurantName, openingHours, categories);

// console.log(typeof restaurantName, typeof openingHours, typeof categories);

// const { menu = [], starterMenu: starters = [] } = restaurant;

// console.log(menu, starters);

// let a = 111;
// let b = 222;

// console.log(a, b);

// const obj = {
//   a: 23,
//   b: 7,
//   c: 14,
// };

// ({ a, b } = obj); // It works now

// console.log(a, b);

// const {
//   fri: { open: o, close: c },
// } = openingHours;

// console.log(o, c);
// console.log('??????????????/');
// console.log(restaurant.orderDelivery);

// // restaurant.orderDelivery('ehe');

// restaurant.orderDelivery({
//   time: '22:30',
//   address: 'Kale Street 22, 214 515',
//   mainIndex: 2,
//   starterIndex: 2,
// });

// restaurant.orderDelivery({
//   address: 'Kale Street 22, 214 515',
//   starterIndex: 1,
// });

// console.log(restaurant.orderDelivery);

// const arr = [2, 3, 4];

// const badNewArray = [1, arr[0], arr[1], arr[2]];

// const goodNewArray = [1, ...arr];

// console.log();

// console.log(arr);
// console.log(badNewArray);
// console.log(goodNewArray);

// console.log(...arr);
// console.log(...goodNewArray);
// console.log(...badNewArray);

// console.table(arr);
// console.table(badNewArray);
// console.table(goodNewArray);

// console.table(...arr);
// console.table(...goodNewArray);
// console.table(...badNewArray);

// const newArray = [...goodNewArray, ...badNewArray, 'EHE'];

// console.log(newArray);

// console.table(newArray);

// console.log(...newArray);

// const newNewArray = [...restaurant.mainMenu, ...restaurant.starterMenu];

// console.log(newNewArray);
// console.table(...newNewArray);

// const str = 'AlzyWelzy';
// const letters = [...str, '', 'A.'];
// console.log(str);
// console.log(letters);
// console.log(...str);
// console.log(...letters);

// const ingredients = [
//   prompt(`Let's make pasta! ing1?`),
//   prompt(`Let's make pasta! ing2?`),
//   prompt(`Let's make pasta! ing3?`),
// ];

// console.log(ingredients);

// restaurant.orderPasta(...ingredients);

// const newRestaurant = { foundedIn: 1996, ...restaurant, founder: 'Owner' };

// console.log(newRestaurant);

// const restaurantCopy = { ...restaurant };
// restaurantCopy.name = 'Ristorante Roma';

// console.table(restaurant);
// console.table(newRestaurant);
// console.table(restaurantCopy);

// Rest Pattern and Parameters
// 1) Destructuring

// SPREAD, because on RIGHT side of =
// const arr = [1, 2, ...[3, 4]];

// REST, because on LEFT side of =
// const [a, b, ...others] = [1, 2, 3, 4, 5];
// console.log(a, b, others);

// const [pizza, , risotto, ...otherFood] = [
//   ...restaurant.mainMenu,
//   ...restaurant.starterMenu,
// ];
// console.log(pizza, risotto, otherFood);

// // Objects
// const { sat, ...weekdays } = restaurant.openingHours;
// console.log("Sat's opening hours", sat, weekdays);
// console.log(sat);
// console.log(weekdays);

// // 2) Functions
// const add = function (...numbers) {
//   let sum = 0;
//   for (let i = 0; i < numbers.length; i++) sum += numbers[i];
//   console.log(sum);
// };

// add(2, 3);
// add(5, 3, 7, 2);
// add(8, 2, 5, 3, 2, 1, 4);

// const x = [23, 5, 7];
// add(...x);

// restaurant.orderPizza('mushrooms', 'onion', 'olives', 'spinach');
// restaurant.orderPizza('mushrooms');

// ///////////////////////////////////////
// // The Spread Operator (...)

// const arr = [7, 8, 9];
// const badNewArr = [1, 2, arr[0], arr[1], arr[2]];
// console.log(badNewArr);

// const newArr = [1, 2, ...arr];
// console.log(newArr);

// console.log(...newArr);
// console.log(1, 2, 7, 8, 9);

// const newMenu = [...restaurant.mainMenu, 'Gnocci'];
// console.log(newMenu);

// // Copy array
// const mainMenuCopy = [...restaurant.mainMenu];

// // Join 2 arrays
// const menu = [...restaurant.starterMenu, ...restaurant.mainMenu];
// console.log(menu);

// // Iterables: arrays, strings, maps, sets. NOT objects
// const str = 'Alzy';
// const letters = [...str, ' ', 'S.'];
// console.log(letters);
// console.log(...str);
// // console.log(`${...str} Welzy`);

// // Real-world example
// const ingredients = [
//   // prompt("Let's make pasta! Ingredient 1?"),
//   // prompt('Ingredient 2?'),
//   // prompt('Ingredient 3'),
// ];
// console.log(ingredients);

// restaurant.orderPasta(ingredients[0], ingredients[1], ingredients[2]);
// restaurant.orderPasta(...ingredients);

// // Objects
// const newRestaurant = { foundedIn: 1998, ...restaurant, founder: 'Guiseppe' };
// console.log(newRestaurant);

// const restaurantCopy = { ...restaurant };
// restaurantCopy.name = 'Ristorante Roma';
// console.log(restaurantCopy.name);
// console.log(restaurant.name); // restaurant is not changed
