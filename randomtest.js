const temperatures_1 = [3, -2, -6, -1, "error", 9, 13, 17, 15, 14, 9, 5];

const temperatures_2 = [3, 5, 1, 100, 90, 30, 50, 70, 80, 90, 1000, 110, 120];

const temperatures_3 = [];

function calcTempAmplitude(temps1, temps2) {
  const temps = temps1.concat(temps2);
  let max = temps[0];
  let min = temps[0];

  for (let i = 0; i < temps.length; i++) {
    const curTemp = temps[i];
    if (typeof curTemp !== "number") continue;

    if (curTemp > max) max = curTemp;
    if (curTemp < min) min = curTemp;
  }
  //   console.log(max, min);
  console.log(`Max: ${max}, Min: ${min}`);
  console.log(`The temperature amplitude is ${max - min}`);
  return max - min;
}

const amplitude = calcTempAmplitude(temperatures_1, temperatures_2);

console.log(amplitude);

const measureKelvin = function () {
  const measurement = {
    type: "temp",
    unit: "celsius",
    value: Number(prompt("Degrees celsius:")),
  };
  console.table(measurement);
  const kelvin = measurement.value + 273;
  return kelvin;
};

console.log(measureKelvin());
