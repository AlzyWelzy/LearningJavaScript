// Declare variables called "country", "continent" and "population" and assign their values according to your own country (population in millions)

// Log their values to the console

const country = "USA";
const continent = "North America";
const population = 328.2;

console.log(`\n`);

console.log(
  `The country is ${country}, the continent is ${continent}, and the population is ${population} million.`
);

console.log(`\n`);
