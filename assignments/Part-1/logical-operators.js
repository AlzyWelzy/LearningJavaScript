// Comment out the previous code so the prompt doesn't get in the way

// Let's say Sarah is looking for a new country to live in. She wants to live in a country that speaks english, has less than 50 million people and is not an island.

// Write an if statement to help Sarah figure out if your country is right for her. You will need to write a condition that accounts for all of Sarah's criteria. Take your time with this, and check part of the solution if necessary.

// If yours is the right country, log a string like this: 'You should live in Portugal :)'. If not, log 'Portugal does not meet your criteria :('

// Probably your country does not meet all the criteria. So go back and temporarily change some variables in order to make the condition true (unless you live in Canada :D)

const name = "Sarah";

// 50 million people = 50000000

const country = String(prompt("Enter your country name: "));
const population = Number(prompt("Enter the population of your country: "));
const language = String(prompt("Enter the language of your country: "));
const isIsland = Boolean(prompt("Is your country an island? (true/false) "));

if (language === "english" && population < 50000000 && !isIsland) {
  console.log(`\n`);
  console.log(`${name}, you should live in ${country} :)`);
  console.log(`\n`);
} else {
  console.log(`\n`);
  console.log(`${country} does not meet your criteria :(`);
  console.log(`\n`);
}
