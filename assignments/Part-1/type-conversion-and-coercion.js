// Predict the result of these 5 operations without executing them:

// 1. '9' - '5'; // 4
// 2. '19' - '13' + '17'; // 617
// 3. '19' - '13' + 17; // 23
// 4. '123' < 57; // false
// 5. 5 + 6 + '4' + 9 - 4 - 2; // 1143

//  Execute the operations to check if you were right

console.log(`\n`);

console.log("9" - "5");
console.log("19" - "13" + "17");
console.log("19" - "13" + 17);
console.log("123" < 57);
console.log(5 + 6 + "4" + 9 - 4 - 2);

console.log(`\n`);
