// Recreate the description variable from the last assignment, this time using the template literal syntax

const country = "India";
const continent = "Asia";
let language = "Hindi";
let population = 1366417754;

// Language after split

let newPopulation_1 = population / 2;
let newPopulation_2 = population / 2;

// Increasing the original population by 1

population = population + 1;

// Population of Finland

let finlandPopulation = 6000000;

// Average population of a country

let averagePopulation = 33000000;

if (population > averagePopulation) {
  console.log(`\n`);
  console.log(`India has more population than Finland.`);
  console.log(`\n`);
}

// description

let description = `${country} is in ${continent}, and its ${population} million people speak ${language}.`;

console.log(description);

console.log(`\n`);
