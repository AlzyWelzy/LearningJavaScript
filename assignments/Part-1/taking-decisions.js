// If your country's population is greater than 33 million, log a string like this to the console: 'Portugal's population is above average'. Otherwise, log a string like 'Portugal's population is 22 million below average' (the 22 is the average of 33 minus the country's population)

// After checking the result, change the population temporarily to 13 and then to 130. See the different results, and set the population back to original

const country = "India";
const continent = "Asia";
const averagePopulation = 33000000;
let language = "Hindi";
let population = 1366417754;
let _13population = 13000000;
let _130population = 130000000;

console.log(`\n`);

if (population > averagePopulation) {
  console.log(`\n`);
  console.log(`${country}'s population is above average.`);
  console.log(`\n`);
}

population = _13population;

console.log(`\n`);

if (population > averagePopulation) {
  console.log(`\n`);
  console.log(`${country}'s population is above average.`);
  console.log(`\n`);
}

population = _130population;

console.log(`\n`);

if (population > averagePopulation) {
  console.log(`\n`);
  console.log(`${country}'s population is above average.`);
  console.log(`\n`);
}

console.log(`\n`);
