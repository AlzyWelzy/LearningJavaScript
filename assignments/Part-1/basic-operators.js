// If your country is split is half, and each half would contain half the population, then how many people live in each half?

//  Increase the population of your country by 1 and log the result to the console

// Finland has a population of 6 million. Does your country have more people than Finland?

// The average population of a country is 33 million people. Does your country have less people than the average country?

// Based on the variables you created, create a new variable 'description' which contains a string with this format: 'Portugal is in Europe, and its 11 million people speak portuguese'

const country = "India";
const continent = "Asia";
let language = "Hindi";
let population = 1366417754;

// Language after split

let newPopulation_1 = population / 2;
let newPopulation_2 = population / 2;

// Increasing the original population by 1

population = population + 1;

// Population of Finland

let finlandPopulation = 6000000;

// Average population of a country

let averagePopulation = 33000000;

if (population > averagePopulation) {
  console.log(`\n`);
  console.log(`India has more population than Finland.`);
  console.log(`\n`);
}

// description

let description =
  country +
  " is in " +
  continent +
  ", and its " +
  population +
  " million people speak " +
  language +
  ".";

console.log(description);

console.log(`\n`);
