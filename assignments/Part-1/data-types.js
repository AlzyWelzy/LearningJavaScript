// Set the value of 'language' to the language spoken where you live (some countries have multiple languages, but just choose one)

// Think about which variables should be const variables (which values will never change, and which might change?). Then, change these variables to const.

// Try to change one of the changed variables now, and observe what happens

let language = "Hindi";
const country = "India";

console.log(`\n`);

console.log(`The country is $(country) and the language is $(language).`);

console.log(`\n`);

language = "Tamil";

console.log(`The country is $(country) and the language is $(language).`);

console.log(`\n`);
