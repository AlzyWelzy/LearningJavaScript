/*
If your country's population is greater than 33 million, use the ternary operator
to log a string like this to the console: 'Portugal's population is above average'.
Otherwise, simply log 'Portugal's population is below average'. Notice how only
one word changes between these two sentences!
*/

/*
After checking the result, change the population temporarily to 13 and then to
130. See the different results, and set the population back to original
*/

const country = String(prompt("Enter your country name: "));
let population = Number(prompt("Enter the population of your country: "));
let averagePopulation = 33000000;

const countryBoolean =
  population >= averagePopulation
    ? `${country}'s population is above average`
    : `${country}'s population is below average`;

console.log(`\n`);

console.log(countryBoolean);

console.log(`\n`);

population = 13000000;

const countryBoolean_1 =
  population >= averagePopulation
    ? `${country}'s population is above average`
    : `${country}'s population is below average`;

console.log(countryBoolean_1);

population = 130000000;

const countryBoolean_2 =
  population >= averagePopulation
    ? `${country}'s population is above average`
    : `${country}'s population is below average`;

console.log(countryBoolean_2);
