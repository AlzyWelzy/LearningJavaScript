/*
Add a method called 'describe' to the 'myCountry' object. This method
will log a string to the console, similar to the string logged in the previous
assignment, but this time using the 'this' keyword.
*/

// Call the 'describe' method

/*
Add a method called 'checkIsland' to the 'myCountry' object. This
method will set a new property on the object, called 'isIsland'.
'isIsland' will be true if there are no neighbouring countries, and false if
there are. Use the ternary operator to set the property.
*/

const myCountry = {
  country: "India",
  capital: "New Delhi",
  language: "Hindi",
  population: 1300000000,
  neighbours: ["Bangladesh", "Nepal", "Bhutan"],
  describe: function () {
    return `${this.country} has ${this.population} people, their mother tongue is ${this.language}, they have ${this.neighbours.length} neighbouring countries and a capital called ${this.capital}.`;
  },
  checkIsland: function () {
    this.isIsland = this.neighbours.length === 0 ? true : false;
  },
};

console.log(myCountry.describe());
myCountry.checkIsland();
console.log(myCountry.isIsland);
