/*
Create a function called 'describePopulation'. Use the function type you
like the most. This function takes in two arguments: 'country' and
'population', and returns a string like this: 'China has 1441 million people,
which is about 18.2% of the world.'
*/

/*
To calculate the percentage, 'describePopulation' call the
'percentageOfWorld1' you created earlier
*/

/*
Call 'describePopulation' with data for 3 countries of your choice
*/

function describePopulation(country, population) {
  return `${country} has ${population} million people, which is about ${percentageOfWorld1(
    country,
    population
  )} of the world.`;
}

function percentageOfWorld1(country, population) {
  return `${(population / 7900).toPrecision(4) * 100}%`;
}

const India = describePopulation("India", 1300);
const Bangladesh = describePopulation("Bangladesh", 160);
const Pakistan = describePopulation("Pakistan", 200);

console.log(`\n`);
console.log(India);
console.log(`\n`);
console.log(Bangladesh);
console.log(`\n`);
console.log(Pakistan);
console.log(`\n`);
