/*
Recreate the challenge from the lecture 'Looping Arrays, Breaking and Continuing',
but this time using a while loop (call the array 'percentages3')
*/

/*
Reflect on what solution you like better for this task: the for loop or the while
loop?
*/

const populations = [1300, 160, 200, 100];

const percentages = ["16.46%", "2.025%", "2.532%", "1.266%"];

const percentages3 = [];

let i = 0;

while (i < populations.length) {
  percentages3.push(percentageOfWorld(populations[i]));
  i++;
}

function percentageOfWorld(population) {
  return `${(population / 7900).toPrecision(4) * 100}%`;
}

console.log(percentages3);

if (percentages3.length === percentages.length) {
  console.log("Same length");

  for (let i = 0; i < percentages3.length; i++) {
    if (percentages3[i] === percentages[i]) {
      console.log("Same value");
    } else {
      console.log("Different value");
    }
  }
}
