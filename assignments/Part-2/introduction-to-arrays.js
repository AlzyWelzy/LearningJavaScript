/*
Create an array containing 4 population values of 4 countries of your choice.
You may use the values you have been using previously. Store this array into a
variable called 'populations'
*/

/*
Log to the console whether the array has 4 elements or not (true or false)
*/

/*
Create an array called 'percentages' containing the percentages of the
world population for these 4 population values. Use the function
'percentageOfWorld1' that you created earlier to compute the 4
percentage values
*/

const country = ["India", "Bangladesh", "Pakistan", "Nepal"];

const populations = [1300, 160, 200, 100];

console.log(populations.length === 4);

function percentageOfWorld(country, population) {
  return `${(population / 7900).toPrecision(4) * 100}%`;
}

const percentages = [
  percentageOfWorld(country[0], populations[0]),
  percentageOfWorld(country[1], populations[1]),
  percentageOfWorld(country[2], populations[2]),
  percentageOfWorld(country[3], populations[3]),
];

function describePopulation(country, population, percentage) {
  return `${country} has ${population} million people, which is about ${percentage} of the world.`;
}

console.log(describePopulation(country[0], populations[0], percentages[0]));
console.log(describePopulation(country[1], populations[1], percentages[1]));
console.log(describePopulation(country[2], populations[2], percentages[1]));
console.log(describePopulation(country[3], populations[3], percentages[1]));

console.log(percentages);
