/*
The world population is 7900 million people. Create a function declaration
called 'percentageOfWorld1' which receives a 'population' value, and
returns the percentage of the world population that the given population
represents. For example, China has 1441 million people, so it's about 18.2% of
the world population
*/

/*
To calculate the percentage, divide the given 'population' value by 7900
and then multiply by 100
*/

/*
Call 'percentageOfWorld1' for 3 populations of countries of your choice,
store the results into variables, and log them to the console
*/

/*
Create a function expression which does the exact same thing, called
'percentageOfWorld2', and also call it with 3 country populations (can be
the same populations)
*/

function percentageOfWorld1(country, population) {
  return `${country} has ${population} million people, so it\'s about ${
    (population / 7900).toPrecision(4) * 100
  }% of the world population`;
}

const India = percentageOfWorld1("India", 1300);
const Bangladesh = percentageOfWorld1("Bangladesh", 160);
const Pakistan = percentageOfWorld1("Pakistan", 200);

console.log(`\n`);

console.log(India);
console.log(`\n`);
console.log(Bangladesh);
console.log(`\n`);
console.log(Pakistan);
console.log(`\n`);
