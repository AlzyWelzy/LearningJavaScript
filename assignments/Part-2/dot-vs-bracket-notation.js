/*
Using the object from the previous assignment, log a string like this to the
console: 'Finland has 6 million finnish-speaking people, 3 neighbouring countries
and a capital called Helsinki.'
*/

/*
Increase the country's population by two million using dot notation, and then
decrease it by two million using brackets notation.
*/

const myCountry = {
  country: "India",
  capital: "New Delhi",
  language: "Hindi",
  population: 1300000000,
  neighbours: ["Bangladesh", "Nepal", "Bhutan"],
};

const describeCountry = function (country, population, capital, language) {
  return `${country} has ${population} people, their mother tongue is ${language}, they have 3 neighbouring countries and a capital called ${capital}.`;
};

const description = describeCountry(
  myCountry.country,
  myCountry.population,
  myCountry.capital,
  myCountry.language
);

console.log("\n");
console.log(description);
console.log("\n");

myCountry.population += 2000000;
console.log(myCountry.population);

myCountry["population"] -= 2000000;
console.log(myCountry.population);
