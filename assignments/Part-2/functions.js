/*
Write a function called 'describeCountry' which takes three parameters:
'country', 'population' and 'capitalCity'. Based on this input, the
function returns a string with this format: 'Finland has 6 million people and its
capital city is Helsinki'
*/

/*
Call this function 3 times, with input data for 3 different countries. Store the
returned values in 3 different variables, and log them to the console
*/

function describeCountry(country, population, capitalCity) {
  return `${country} has ${population} million people and its capital city is ${capitalCity}`;
}

const India = describeCountry("India", 1300, "New Delhi");
const Bangladesh = describeCountry("Bangladesh", 160, "Dhaka");
const Pakistan = describeCountry("Pakistan", 200, "Islamabad");

console.log(`\n`);
console.log(India);
console.log(`\n`);
console.log(Bangladesh);
console.log(`\n`);
console.log(Pakistan);
console.log(`\n`);
