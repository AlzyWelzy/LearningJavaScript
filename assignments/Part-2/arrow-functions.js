/*
Recreate the last assignment, but this time create an arrow function called
'percentageOfWorld3'
*/

const percentageOfWorld3 = (country, population) => {
  return `${country} has ${population} million people, so it\'s about ${
    (population / 7900).toPrecision(4) * 100
  }% of the world population`;
};

console.log(`\n`);

console.log(percentageOfWorld3("India", 1300));
console.log(percentageOfWorld3("Bangladesh", 160));
console.log(percentageOfWorld3("Pakistan", 200));

console.log(`\n`);
