// Let's bring back the 'populations' array from a previous assignment

/*
Use a for loop to compute an array called 'percentages2' containing the
percentages of the world population for the 4 population values. Use the
function 'percentageOfWorld1' that you created earlier
*/

/*
Confirm that 'percentages2' contains exactly the same values as the
'percentages' array that we created manually in the previous assignment,
and reflect on how much better this solution is
*/

const populations = [1300, 160, 200, 100];

const percentages = ["16.46%", "2.025%", "2.532%", "1.266%"];

const percentages2 = [];

for (let i = 0; i < populations.length; i++) {
  percentages2.push(percentageOfWorld(populations[i]));
}

function percentageOfWorld(population) {
  return `${(population / 7900).toPrecision(4) * 100}%`;
}

console.log(percentages2);

if (percentages2.length === percentages.length) {
  console.log("Same length");

  for (let i = 0; i < percentages2.length; i++) {
    if (percentages2[i] === percentages[i]) {
      console.log("Same value");
    } else {
      console.log("Different value");
    }
  }
}
