"use strict";

const weekdays = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

// Data needed for a later exercise
const flights =
  "_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30";

// Data needed for first part of the section

const openingHours = {
  [weekdays[3]]: {
    open: 12,
    close: 22,
  },
  [weekdays[4]]: {
    open: 11,
    close: 23,
  },
  [weekdays[5]]: {
    open: 0, // Open 24 hours
    close: 24,
  },
};

const restaurant = {
  name: "Classico Italiano",
  location: "Via Angelo Tavanti 23, Firenze, Italy",
  categories: ["Italian", "Pizzeria", "Vegetarian", "Organic"],
  starterMenu: ["Focaccia", "Bruschetta", "Garlic Bread", "Caprese Salad"],
  mainMenu: ["Pizza", "Pasta", "Risotto"],
  openingHours,
  order(starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  orderDelivery({ starterIndex = 1, mainIndex = 0, address, time = `20:00` }) {
    console.log(
      `Order received ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`
    );
  },

  orderPasta({ ing1, ing2, ing3 }) {
    console.log(`Here is your pasta with ${ing1},${ing2} and ${ing3}`);
  },

  orderPizza({ mainIngredient, ...otherIngredients }) {
    console.log(mainIngredient);
    console.log(otherIngredients);
  },
};

console.log(flights);
console.log(flights.split(`+`));

flights.split(`+`).forEach(function (flight) {
  console.log(flight);
  console.log(flight.split(`;`));
  const [type, from, to, time] = flight.split(`;`);
  const output = `${type.startsWith(`_Delayed`) ? `🔴` : ``}${type.replaceAll(
    `-`,
    ``
  )} ${getCode(from)} ${getCode(to)} (${time.replace(`:`, `h`)})`.padStart(36);

  console.log(output);
});
