const data = [
  [17, 21, 23],
  [12, 5, -5, 0, 4],
]; // We gonna use this data

const printForecast = function (arr) {
  let str = "";
  for (let i = 0; i < arr.length; i++) {
    str += `${arr[i]}°C in ${i + 1} days ... `;
  }
  console.log(str);
};

printForecast(data[0]);
printForecast(data[1]);
