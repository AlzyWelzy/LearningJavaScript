/*
Coding Challenge #3
Let's go back to Mark and John comparing their BMIs! This time, let's use objects to
implement the calculations! Remember: BMI = mass / height ** 2 = mass
/ (height * height) (mass in kg and height in meter)
Your tasks:
1.2.3.For each of them, create an object with properties for their full name, mass, and
height (Mark Miller and John Smith)
Create a 'calcBMI' method on each object to calculate the BMI (the same
method on both objects). Store the BMI value to a property, and also return it
from the method
Log to the console who has the higher BMI, together with the full name and the
respective BMI. Example: "John's BMI (28.3) is higher than Mark's (23.9)!"
Test data: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95 m
tall.
*/

function BMIcalc(mass, height) {
  return (mass / height ** 2).toFixed(2);
}

const data = {
  mark: {
    fullName: "Mark Miller",
    mass: 78,
    height: 1.69,
    calcBMI: function () {
      this.bmi = BMIcalc(this.mass, this.height);
      return this.bmi;
    },
  },
  john: {
    fullName: "John Smith",
    mass: 92,
    height: 1.95,
    calcBMI: function () {
      this.bmi = BMIcalc(this.mass, this.height);
      return this.bmi;
    },
  },
};

data.mark.bmi > data.john.bmi
  ? console.log(
      `${data.mark.fullName}'s BMI (${data.mark.bmi}) is higher than ${data.john.fullName}'s (${data.john.bmi})!`
    )
  : console.log(
      `${data.john.fullName}'s BMI (${data.john.bmi}) is higher than ${data.mark.fullName}'s (${data.mark.bmi})!`
    );
