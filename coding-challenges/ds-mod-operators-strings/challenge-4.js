/*
Write a program that receives a list of variable names written in underscore_case
and convert them to camelCase.
The input will come from a textarea inserted into the DOM (see code below to
insert the elements), and conversion will happen when the button is pressed.
Test data (pasted to textarea, including spaces):
underscore_case
first_name
Some_Variable
calculate_AGE
delayed_departure
Should produce this output (5 separate console.log outputs):
underscoreCase
firstName
someVariable
calculateAge
delayedDeparture
✅
✅✅
✅✅✅
✅✅✅✅
✅✅✅✅✅
Hints:
§
§
§
§
Remember which character defines a new line in the textarea 😉
The solution only needs to work for a variable made out of 2 words, like a_b
Start without worrying about the ✅. Tackle that only after you have the variable
name conversion working 😉
This challenge is difficult on purpose, so start watching the solution in case
you're stuck. Then pause and continue!
*/

document.body.append(document.createElement("textarea"));
document.body.append(document.createElement("button"));

const btn = document.querySelector("button");

const formatVariables = function () {
  const data = document.querySelector("textarea").value;
  console.log(data);
  const textArr = data.split("\n");
  console.log(textArr);
  textArr.forEach((word, index, arr) => {
    word = word.toLowerCase().trim();
    let splitWord = word.split("_");
    if (!splitWord[0]) splitWord.shift();
    splitWord.forEach((split, index, arr) => {
      if (index !== 0) split = split.replace(split[0], split[0].toUpperCase());
      arr[index] = split;
    });
    word = splitWord.join("");
    arr[index] = word;
    console.log(word.padEnd(20).padEnd(20 + index + 1, "✅"));
  });
  console.log(textArr);
};
btn.addEventListener("click", formatVariables);
