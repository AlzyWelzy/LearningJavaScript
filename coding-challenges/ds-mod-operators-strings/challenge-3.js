const gameEvents = new Map([
  [17, "⚽ GOAL"],
  [36, "🔁 Substitution"],
  [47, "⚽ GOAL"],
  [61, "🔁 Substitution"],
  [64, "🔶 Yellow card"],
  [69, "🔴 Red card"],
  [70, "🔁 Substitution"],
  [72, "🔁 Substitution"],
  [76, "⚽ GOAL"],
  [80, "⚽ GOAL"],
  [92, "🔶 Yellow card"],
]);

// Task 1
// const events = new Set();

// for (const [key, value] of gameEvents) {
//   console.log(value);
//   events.add(value);
// }

// const eventsArr = [...events];
// console.log(events);
// console.log(eventsArr);

// Task 1
const events = [...new Set(gameEvents.values())];

// Task 2
// events.delete("🔶 Yellow card");
// console.log(events);

// Task 2
gameEvents.delete(64);

// Task 3
// const lastTime = [...new Set(gameEvents.keys())];
// console.log(lastTime);

// console.log(
//   `An event happened, on average, every ${(
//     lastTime[lastTime.length - 1] / gameEvents.size
//   ).toFixed(2)} minutes`
// );

// Task 3
let totalTime = 90;
console.log(`An event happened, every ${totalTime / gameEvents.size} minutes`);

const time = [...gameEvents.keys()].pop();
console.log(`An event happened, every ${time / gameEvents.size} minutes`);

// Task 4
// for (const [key, value] of gameEvents) {
//   console.log(`[${key < 45 ? "FIRST HALF" : "SECOND HALF"}] ${key}: ${value}`);
// }

// Task 4
for (const [min, event] of gameEvents) {
  const half = min <= 45 ? `FIRST` : `SECOND`;
  console.log(`[${half} HALF] ${min}: ${event}`);
}
