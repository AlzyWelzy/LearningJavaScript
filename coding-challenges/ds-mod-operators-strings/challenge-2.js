const avgCalc = (...numbers) => {
  console.log(numbers);
  let total = 0;
  numbers.forEach((element) => (total += element));
  return (total / numbers.length).toFixed(2);
};

const game = {
  team1: "Bayern Munich",
  team2: "Borrussia Dortmund",
  players: [
    [
      "Neuer",
      "Pavard",
      "Martinez",
      "Alaba",
      "Davies",
      "Kimmich",
      "Goretzka",
      "Coman",
      "Muller",
      "Gnarby",

      "Lewandowski",
    ],
    [
      "Burki",
      "Schulz",
      "Hummels",
      "Akanji",
      "Hakimi",
      "Weigl",
      "Witsel",
      "Hazard",
      "Brandt",
      "Sancho",
      "Gotze",
    ],
  ],
  score: "4:0",
  scored: ["Lewandowski", "Gnarby", "Lewandowski", "Hummels"],
  date: "Nov 9th, 2037",

  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
};

const players1 = [...game.players[0]];
const players2 = [...game.players[1]];

const [gk, ...fieldPlayers] = players1;
console.log(gk, fieldPlayers);

const allPlayers = [...players1, ...players2];
console.log(allPlayers);

const players1Final = [...players1, "Thiago", "Coutinho", "Perisic"];
console.log(players1Final);

const {
  odds: { team1, x: draw, team2 },
} = game;
console.log(team1, draw, team2);

const printGoals = function (...players) {
  console.log(`${players.length} goals were scored`);
};

printGoals("Davies", "Muller", "Lewandowski", "Kimmich");

team1 < team2 && console.log("Team 1 is more likely to win");
team1 > team2 && console.log("Team 2 is more likely to win");

console.log(game.scored);

for (const score of game.scored) {
  console.log(score);
}

const score = game.scored;
console.log(score);

console.log(game.odds);

for (const odd of Object.entries(game.odds)) {
  console.log(odd);
}

const odd = Object.entries(game.odds);

console.log(odd);

const oddAvg = [];

for (const avg of odd) {
  oddAvg.push(avg[1]);
}

console.log(oddAvg);

avgCalc(...oddAvg);

const totalAvg = avgCalc(...oddAvg);

console.log(totalAvg);

console.log(odd);

let i = 1;

for (const result of odd) {
  console.log(
    `Odd of ${result[0] != "x" ? `Victory ${game[`team${i}`]}` : "draw"}: ${
      result[1]
    }`
  );
  i++;
}

const scorers = {};

for (const num of game.scored) {
  //   scorers[num] = scorers[num] ? scorers[num] + 1 : 1;
  console.log(num);
  scorers[num]++ || (scorers[num] = 1);
}

console.log(scorers);

const scored = game.scored.entries();

for (const [i, player] of scored) {
  console.log(`Goal ${i + 1}: ${player}`);
}
