const Car = function (make, speed) {
  this.make = make;
  this.speed = speed;
};

const bmw = new Car("BMW", 120);
const mercedes = new Car("Mercedes", 95);

Car.prototype.accelerate = function () {
  this.speed += 10;
  console.log(`${this.make} is going at ${this.speed}`);
  //   return this.speed;
};

bmw.accelerate();
bmw.accelerate();
bmw.accelerate();
bmw.accelerate();
bmw.brake();
bmw.accelerate();
bmw.accelerate();
bmw.accelerate();
bmw.accelerate();
bmw.accelerate();
mercedes.accelerate();

console.log(bmw);
console.log(mercedes);
console.log(mercedes);
console.log(mercedes);
console.log(mercedes);
console.log(mercedes);
mercedes.brake();
console.log(mercedes);
console.log(mercedes);
console.log(mercedes);
console.log(mercedes);

Car.prototype.brake = function () {
  this.speed -= 5;
  console.log(`${this.make} is going at ${this.speed}`);
  //   return this.speed;
};

bmw.brake();
mercedes.brake();

console.log(bmw);
console.log(mercedes);
