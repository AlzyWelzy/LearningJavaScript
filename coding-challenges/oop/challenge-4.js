class Car {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }
  #charge;
  accelerate() {
    if (this.isEV) {
      this.speed += 20;
      this.#charge -= 0.01;
      console.log(
        `${this.make} is going at ${
          this.speed
        } km/h, with a charge of ${Math.floor(this.#charge * 100)}%`
      );
      return this;
    } else {
      this.speed += 10;
      console.log(`${this.make} is going at ${this.speed} km/h`);
      return this;
    }
  }

  brake() {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed} km/h`);
    return this;
  }
}

class EV extends Car {
  #charge;
  constructor(make, speed, charge) {
    super(make, speed);
    this.isEV = true;
    this.#charge = charge;
  }

  chargeBattery(chargeTo) {
    this.#charge = chargeTo / 100;
    return this;
  }
}

// EV.prototype = Object.create(Car.prototype);
// EV.prototype.constructor = Car;

const mitsubishi = new Car("Mitsubishi", 120); // output: "Mitsubishi going at 130."
const tesla = new EV("Tesla", 120, 0.23); // output: "Tesla going at 140, with a charge of 22%"

tesla.chargeBattery(90).brake().accelerate().brake().chargeBattery(9000);
console.log(tesla);

// mitsubishi.chargeBattery(90);
mitsubishi.brake().accelerate();
console.log(mitsubishi);

const rivian = new EV("Rivian", 120, 23);
rivian.chargeBattery(90).brake().accelerate();
console.log(rivian);
