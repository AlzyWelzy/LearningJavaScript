const Car = function (make, speed) {
  this.make = make;
  this.speed = speed;
};

Car.prototype.accelerate = function () {
  if (this.isEV) {
    this.speed += 20;
    this.charge -= 0.01;
    console.log(
      `${this.make} is going at ${
        this.speed
      } km/h, with a charge of ${Math.floor(this.charge * 100)}%`
    );
  } else {
    this.speed += 10;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  }
};

Car.prototype.brake = function () {
  this.speed -= 5;
  console.log(`${this.make} is going at ${this.speed} km/h`);
};

const EV = function (make, speed, charge) {
  Car.call(this, make, speed);
  this.isEV = true;
  this.charge = charge;
};

EV.prototype = Object.create(Car.prototype);
EV.prototype.constructor = Car;

EV.prototype.chargeBattery = function (chargeTo) {
  this.charge = chargeTo / 100;
};

const mitsubishi = new Car("Mitsubishi", 120); // output: "Mitsubishi going at 130."
const tesla = new EV("Tesla", 120, 0.23); // output: "Tesla going at 140, with a charge of 22%"

tesla.chargeBattery(90);
console.log(tesla);
tesla.brake();
tesla.accelerate();

// mitsubishi.chargeBattery(90);
console.log(mitsubishi);
mitsubishi.brake();
mitsubishi.accelerate();
