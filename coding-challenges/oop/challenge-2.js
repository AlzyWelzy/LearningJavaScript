class CarCl {
  constructor(make, speed) {
    this.make = make;
    this.speed = speed;
  }

  get speedUS() {
    return this.speed / 1.6;
  }

  set speedUS(speed) {
    this.speed = speed * 1.6;
  }

  get accelerator() {
    return (this.speed += 10);
  }

  get brake() {
    return (this.speed -= 5);
  }
}

const ford = new CarCl("Ford", 120);
console.log(ford);
ford.speedUS = 50;
