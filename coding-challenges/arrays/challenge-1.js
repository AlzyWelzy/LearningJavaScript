const checkDogs = function (dogsJulia, dogsKate) {
  // const shallowJulia = [...dogsJulia];
  const dogsJuliaCorrected = dogsJulia.slice();
  console.log(dogsJuliaCorrected);
  dogsJuliaCorrected.splice(0, 1);
  dogsJuliaCorrected.splice(-2);
  console.log(dogsJuliaCorrected);
  // const allDogs = [...shallowJulia, ...dogsKate];

  const allDogs = dogsJuliaCorrected.concat(dogsKate);

  const dogs = allDogs.forEach((age, i) =>
    age >= 3
      ? console.log(`Dog number ${i + 1} is an adult, and is ${age} years old`)
      : console.log(`Dog number ${i + 1} is a puppy, and is ${age} years old`)
  );
};

const julia = [
  [3, 5, 2, 12, 7],
  [4, 1, 15, 8, 3],
];
const kate = [
  [9, 16, 6, 8, 3],
  [10, 5, 6, 1, 4],
];

checkDogs(julia[0], kate[0]);
checkDogs(julia[1], kate[1]);
