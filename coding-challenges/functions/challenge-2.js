// document.querySelector(`body`).onclick = (function () {
//   const header = document.querySelector(`h1`);
//   header.style.color = `red`;
//   return () => (header.style.color = `blue`);
// })();

// (function () {
//   const header = document.querySelector(`h1`);
//   header.style.color = `red`;

//   document.querySelector(`body`).addEventListener(`click`, function () {
//     header.style.color = `blue`;
//   });
// })();

(() => {
  const header = document.querySelector(`h1`);
  header.style.color = `red`;

  document.querySelector(`body`).addEventListener(`click`, () => {
    header.style.color = `blue`;
  });
})();
