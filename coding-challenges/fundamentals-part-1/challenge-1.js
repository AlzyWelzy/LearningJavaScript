/*
Coding Challenge #1
Mark and John are trying to compare their BMI (Body Mass Index), which is
calculated using the formula:
BMI = mass / height ** 2 = mass / (height * height) (mass in kg
and height in meter).
*/

/*
Your tasks:
1.2.3.Store Mark's and John's mass and height in variables
Calculate both their BMIs using the formula (you can even implement both
versions)
Create a Boolean variable 'markHigherBMI' containing information about
whether Mark has a higher BMI than John.
*/

/*
Test data:
§
 Data 1: Marks weights 78 kg and is 1.69 m tall. John weights 92 kg and is 1.95
m tall.
§
 Data 2: Marks weights 95 kg and is 1.88 m tall. John weights 85 kg and is 1.76
m tall.
*/

const markMass_1 = 78;
const markHeight_1 = 1.69;

const johnMass_1 = 92;
const johnHeight_1 = 1.95;

function calcBMI(mass, height) {
  return (mass / height ** 2).toPrecision(4);
}

const markBMI_1 = calcBMI(markMass_1, markHeight_1);
const johnBMI_1 = calcBMI(johnMass_1, johnHeight_1);

const markHigherBMI_1 = markBMI_1 > johnBMI_1;

console.log("\n");
console.log(markBMI_1, johnBMI_1, markHigherBMI_1);

if (markHigherBMI_1) {
  console.log(
    `Mark's BMI (${markBMI_1}) is higher than John's (${johnBMI_1})!`
  );
} else {
  console.log(
    `John's BMI (${johnBMI_1}) is higher than Mark's (${markBMI_1})!`
  );
}

const markMass_2 = 95;
const markHeight_2 = 1.88;

const johnMass_2 = 85;
const johnHeight_2 = 1.76;

const markBMI_2 = calcBMI(markMass_2, markHeight_2);
const johnBMI_2 = calcBMI(johnMass_2, johnHeight_2);

const markHigherBMI_2 = markBMI_2 > johnBMI_2;

console.log("\n");
console.log(markBMI_2, johnBMI_2, markHigherBMI_2);
console.log("\n");

if (markHigherBMI_2) {
  console.log(
    `Mark's BMI (${markBMI_2}) is higher than John's (${johnBMI_2})!`
  );
} else {
  console.log(
    `John's BMI (${johnBMI_2}) is higher than Mark's (${markBMI_2})!`
  );
}
