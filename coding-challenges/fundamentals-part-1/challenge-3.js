/*
Coding Challenge #3
There are two gymnastics teams, Dolphins and Koalas. They compete against each
other 3 times. The winner with the highest average score wins a trophy!
Your tasks:
1.2.3.4.Calculate the average score for each team, using the test data below
Compare the team's average scores to determine the winner of the competition,
and print it to the console. Don't forget that there can be a draw, so test for that
as well (draw means they have the same average score)
Bonus 1: Include a requirement for a minimum score of 100. With this rule, a
team only wins if it has a higher score than the other team, and the same time a
score of at least 100 points. Hint: Use a logical operator to test for minimum
score, as well as multiple else-if blocks 😉
Bonus 2: Minimum score also applies to a draw! So a draw only happens when
both teams have the same score and both have a score greater or equal 100
points. Otherwise, no team wins the trophy
Test data:
§
 Data 1: Dolphins score 96, 108 and 89. Koalas score 88, 91 and 110
§
 Data Bonus 1: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 123
§
 Data Bonus 2: Dolphins score 97, 112 and 101. Koalas score 109, 95 and 106
*/

const calcAverage = (score1, score2, score3) =>
  ((score1 + score2 + score3) / 3).toPrecision(4);

const checkWinner = (avgDolphins, avgKoalas) => {
  console.log(`Dolphins: ${avgDolphins}, Koalas: ${avgKoalas}`);

  if (avgDolphins >= 100 && avgKoalas >= 100) {
    if (avgDolphins > avgKoalas) {
      console.log(`Dolphins win!`);
    } else if (avgKoalas > avgDolphins) {
      console.log(`Koalas win!`);
    } else {
      console.log(`It's a draw!`);
    }
  } else {
    console.log(`No team wins the trophy`);
  }
};

const data1 = {
  dolphins: [96, 108, 89],
  koalas: [88, 91, 110],
};

const data2 = {
  dolphins: [97, 112, 101],
  koalas: [109, 95, 123],
};

const data3 = {
  dolphins: [97, 112, 101],
  koalas: [109, 95, 106],
};

const avgDolphins1 = calcAverage(...data1.dolphins);
const avgKoalas1 = calcAverage(...data1.koalas);

const avgDolphins2 = calcAverage(...data2.dolphins);
const avgKoalas2 = calcAverage(...data2.koalas);

const avgDolphins3 = calcAverage(...data3.dolphins);
const avgKoalas3 = calcAverage(...data3.koalas);

checkWinner(avgDolphins1, avgKoalas1);
checkWinner(avgDolphins2, avgKoalas2);
checkWinner(avgDolphins3, avgKoalas3);
