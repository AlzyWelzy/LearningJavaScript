// Coding Challenge #2

// Use the BMI example from Challenge #1, and the code you already wrote, and improve it.

// Your tasks:

// 1. Print a nice output to the console, saying who has the higher BMI. The message is either "Mark's BMI is higher than John's!" or "John's BMI is higher than Mark's!"

// 2. Use a template literal to include the BMI values in the outputs. Example: "Mark's BMI (28.3) is higher than John's (23.9)!"

// Hint: Use an if/else statement 😉

const markMass_1 = 78;
const markHeight_1 = 1.69;

const johnMass_1 = 92;
const johnHeight_1 = 1.95;

function calcBMI(mass, height) {
  return (mass / height ** 2).toPrecision(4);
}

function printBMI(mass, height) {
  const bmi = calcBMI(mass, height);
  console.log(`BMI: ${bmi}`);
}

printBMI(markMass_1, markHeight_1);

printBMI(johnMass_1, johnHeight_1);

const markBMI_1 = calcBMI(markMass_1, markHeight_1);
const johnBMI_1 = calcBMI(johnMass_1, johnHeight_1);

if (markBMI_1 > johnBMI_1) {
  console.log(
    `Mark's BMI (${markBMI_1}) is higher than John's (${johnBMI_1})!`
  );
}

const markMass_2 = 95;
const markHeight_2 = 1.88;

const johnMass_2 = 85;
const johnHeight_2 = 1.76;

const markBMI_2 = calcBMI(markMass_2, markHeight_2);
const johnBMI_2 = calcBMI(johnMass_2, johnHeight_2);

if (markBMI_2 > johnBMI_2) {
  console.log(
    `Mark's BMI (${markBMI_2}) is higher than John's (${johnBMI_2})!`
  );
}
