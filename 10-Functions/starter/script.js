'use strict';

// const bookings = [];

// const createBooking = function (
//   flightNum = `007`,
//   numPassengers = 1,
//   price = 199 * numPassengers
// ) {
//   // Old Way (ES5)
//   //   flightNum = `007`;
//   //   numPassengers = numPassengers || 1;
//   //   price = price || 199;

//   // New Way (ES6)
//   // Look at the parameters

//   const booking = {
//     flightNum,
//     numPassengers,
//     price,
//   };

//   console.log(booking);
//   bookings.push(booking);
// };

// createBooking(`LH123`);
// createBooking(`LH123`, 2, 800);
// createBooking(`LH123`, 10, 2000);

// // To escape parameters

// createBooking(`LM123`, undefined, 1000);

// const flight = `LH234`;
// const Alzy = {
//   name: `Alzy Welzy`,
//   passport: 32432479824792,
// };

// const checkIn = function (flightNum, passenger) {
//   flightNum = `LH999`;
//   passenger.name = `Mr. ${passenger.name}`;

//   if (passenger.passport === 32432479824792) {
//     alert(`Checked in`);
//   } else {
//     alert(`Wrong Passport!`);
//   }
// };

// checkIn(flight, Alzy);
// console.log(flight);
// console.log(Alzy);

// // Is the same as doing
// const flightNum = flight;
// const passenger = Alzy;

// const newPassport = function (person) {
//   person.passport = Math.trunc(Math.random() * 100000000000000000);
// };

// newPassport(Alzy);
// checkIn(flight, Alzy);

// const oneWord = function (str) {
//   return str.replace(/ /g, ``).toLowerCase();
// };

// oneWord(`ehe te nandayo`);

// const upperFirstWord = function (str) {
//   const [first, ...others] = str.split(` `);
//   console.log(first);
//   console.log(others);
//   return [first.toUpperCase(), ...others].join(` `);
// };

// upperFirstWord(`ehe te nandayo!`);

// console.log(oneWord(`ehe te nandayo`));

// console.log(upperFirstWord(`ehe te nandayo!`));

// const transformer = function (str, fn) {
//   console.log(`Original String: ${str}`);
//   console.log(`Transformed String: ${fn(str)}`);

//   console.log(`Transformed by: ${fn.name}`);
// };

// transformer(
//   `JavaScript is the Best Programming Language Ever!`,
//   upperFirstWord
// );
// transformer(`JavaScript is the Best Programming Language Ever!`, oneWord);

// const high5 = function () {
//   console.log(`✋`);
// };

// document.addEventListener(`click`, high5);

// [`Alzy`, `Welzy`, `AlzyWelzy`].forEach(high5);

// const greet = function (greeting) {
//   return function (name) {
//     console.log(`${greeting} ${name}`);
//   };
// };

// const greeter = greet(`Hey`);
// console.log(greeter);
// greeter(`AlzyWelzy`);
// greeter(`WelzyAlzy`);

// greet(`Hey`)(`WelzyAlzy`);

// const greetChallenge = greeting => name => console.log(`${greeting} ${name}`);

// const greeterChallenge = greetChallenge(`Hey`);
// console.log(greeterChallenge);
// greeterChallenge(`AlzyWelzy`);
// greeterChallenge(`WelzyAlzy`);
// greetChallenge(`AlzyWelzy`)(`WelzyAlzy`);

// const lufthansa = {
//   airline: `Lufthansa`,
//   iataCode: `LH`,
//   bookings: [],
//   book(flightNum, name) {
//     console.log(
//       `${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`
//     );
//     this.bookings.push({ flight: `${this.iataCode}${flightNum}`, name });
//   },
// };

// lufthansa.book(239, `Alzy Welzy`);
// lufthansa.book(635, `Alzy Welzy`);
// console.log(lufthansa);

// const eurowings = {
//   airline: `Eurowings`,
//   iataCode: `EW`,
//   bookings: [],
// };

// const book = lufthansa.book;
// console.log(book);

// // Does not work
// // book(23, `Manvendra Rajpoot`);

// // Call Method
// book.call(eurowings, 23, `Manvendra Rajpoot`);
// console.log(eurowings);

// const swiss = {
//   airline: `Swiss Air Lines`,
//   iataCode: `LX`,
//   bookings: [],
// };

// book.call(swiss, 12312, `Ankit Rajpoot`);

// // Apply Method
// const flightData = [583, `Manvendra Singh Rajpoot`];
// book.apply(swiss, flightData);
// console.log(swiss);

// book.call(swiss, ...flightData);

// // Bind Method
// const bookEW = book.bind(eurowings);
// const bookLH = book.bind(lufthansa);
// bookEW(23, `Ankit Lodhi`);

// const bookEW23 = book.bind(eurowings, 23);
// bookEW23(`Alzy Welzy`);

// // With Event Listeners
// lufthansa.planes = 300;
// lufthansa.buyPlane = function () {
//   console.log(this);
//   this.planes++;
//   console.log(this.planes);
// };
// document
//   .querySelector(`.buy`)
//   .addEventListener(`click`, lufthansa.buyPlane.bind(lufthansa));

// // Partial Application

// const addTax = (rate, value) => {
//   console.log(rate, value);
//   return value + value * rate;
// };

// console.log(addTax(10, 100));

// const addVAT = addTax.bind(null, 0.23);
// // addTax = value => value + value * 0.23;

// console.log(addVAT(100));
// console.log(addVAT(23));

// // const addTaxRate = function (rate) {
// //   return function (value) {
// //     return value + value * rate;
// //   };
// // };

// const addTaxRate = rate => value => value + value * rate;

// const addVATRate = addTaxRate(0.23);
// console.log(addVATRate(100));
// console.log(addVATRate(23));

// const runOnce = function () {
//   console.log(`This will never run again`);
// };
// runOnce();

// // IIFE Immediately Invoke Function Expression
// (function () {
//   console.log(`This will never run again`);
// })();

// (() => console.log(`This will also never run again`))();

// {
//   const isPrivate = 23;
//   var notPrivate = 46;
// }

// console.log(notPrivate);

// const secureBooking = function () {
//   let passengerCount = 0;

//   return function () {
//     passengerCount++;
//     console.log(`${passengerCount} passengers`);
//   };
// };

// const booker = secureBooking();

// booker();
// booker();
// booker();

// for (var i = 0; i < 3; i++) {
//   const log = () => {
//     console.log(i);
//   };

//   setTimeout(log, 100);
// }

// Example 1.
let f;

const g = function () {
  const a = 555;
  f = function () {
    console.log(a * 2);
  };
};

const h = function () {
  const b = 777;
  f = function () {
    console.log(b * 2);
  };
};

g();
f();
console.dir(f);

// Re-assignment f Function
h();
f();
console.dir(f);

// Example 2.
const boardPassengers = function (n, wait) {
  const perGroup = n / 3;

  setTimeout(function () {
    console.log(`We are now boarding all ${n} passengers`);
    console.log(`There are 3 groups, each with ${perGroup} passengers`);
  }, wait * 1000);

  console.log(`Will start boarding in ${wait} seconds`);
};

const perGroup = 1000;
boardPassengers(180, 3);
