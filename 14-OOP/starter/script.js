// 'use strict';

// const Person = function (firstName, birthYear) {
//   this.firstName = firstName;
//   this.birthYear = birthYear;
// };

// const alzy = new Person(`Alzy`, 2002);
// console.log(alzy);

// const welzy = new Person(`Welzy`, 2002);
// console.log(welzy);

// console.log(alzy, welzy);

// const alzywelzy = 'AlzyWelzy';

// console.log(alzy instanceof Person);
// console.log(alzywelzy instanceof Person);

// // Person.hey();

// // alzy.hey();

// // 1. New {} is created
// // 2. function is called, this = {}
// // 3. {} linked to prototype
// // 4. function automatically return {}

// // Prototypes
// console.log(Person.prototype);

// Person.prototype.calcAge = function () {
//   const curYear = new Date().getFullYear();
//   console.log(curYear - this.birthYear);
//   return curYear - this.birthYear;
// };

// alzy.calcAge();
// welzy.calcAge();

// console.log(alzy.__proto__);
// console.log(alzy.__proto__ === Person.prototype);

// console.log(Person.prototype.isPrototypeOf(alzy));
// console.log(Person.prototype.isPrototypeOf(welzy));
// console.log(Person.prototype.isPrototypeOf(alzywelzy));

// // .prototypeOfLinkedObjects

// Person.prototype.species = 'Homo Spaiens';

// console.log(alzy.species, welzy.species);

// console.log(alzy.hasOwnProperty('firstName'));
// console.log(alzy.hasOwnProperty('birthYear'));
// console.log(alzy.hasOwnProperty('species'));
// console.log(alzy.hasOwnProperty('calcAge'));

// console.log(alzy.__proto__);
// console.log(alzy.__proto__.__proto__);
// console.log(alzy.__proto__.__proto__.__proto__);

// console.log(Person.prototype.constructor);
// console.dir(Person.prototype.constructor);

// const arr = [3, 6, 4, 5, 6, 9, 3];
// console.log(arr.__proto__);
// console.log(arr.__proto__ === Array.prototype);

// console.log(arr.__proto__.__proto__);

// Array.prototype.unique = function () {
//   return [...new Set(this)];
// };

// console.log(arr.unique());

// const h1 = document.querySelector('h1');

// console.log(h1);
// console.log(h1.__proto__);

// console.dir(x => x + 1);

// // class expression
// // const PersonCl = class {};

// // class declaration
// class PersonCl {
//   constructor(fullName, birthYear) {
//     console.log(this);
//     this.fullName = fullName;
//     this.birthYear = birthYear;
//   }

//   calcAge() {
//     const curYear = new Date().getFullYear();
//     return curYear - this.birthYear;
//   }

//   greet() {
//     return `Hey ${this.fullName}!`;
//   }

//   get age() {
//     const curYear = new Date().getFullYear();
//     return curYear - this.birthYear;
//   }

//   set fullName(name) {
//     console.log(name);
//     if (name.includes(` `)) this._fullName = name;
//     else alert(`${name} is not a full name`);
//   }

//   get fullName() {
//     return this._fullName;
//   }

//   static hey() {
//     console.log('Hey THere ');
//     console.log(this);
//   }
// }

// const alsica = new PersonCl('alsica', 2002);
// console.log(alsica);
// alsica.calcAge();
// console.log(alsica.age);

// console.log(alsica.__proto__ === PersonCl.prototype);

// const account = {
//   owner: 'Alzy',
//   movements: [2000, 1000, 20023, 1231, 69420],

//   get latest() {
//     return this.movements.slice(-1).pop();
//   },

//   set latest(mov) {
//     this.movements.push(mov);
//   },
// };

// console.log(account.latest);

// account.latest = 50;

// console.log(account.movements);

// console.log();

// const walter = new PersonCl('Waiter', 1969);
// const walterFull = new PersonCl('Waiter White', 1969);

// walterFull.fullName;

// PersonCl.hey();

// const PersonProto = {
//   calcAge() {
//     const curYear = new Date().getFullYear();
//     return curYear - this.birthYear;
//   },

//   init(firstName, birthYear) {
//     this.firstName = firstName;
//     this.birthYear = birthYear;
//   },
// };

// const steven = Object.create(PersonProto);

// console.log(steven);

// steven.name = 'Steven';
// steven.birthYear = 2002;

// console.log(steven);
// console.log(steven.calcAge());

// const sarah = Object.create(PersonProto);
// sarah.init('Sarah', 1979);

// sarah.calcAge();

// const Person = function (firstName, birthYear) {
//   this.firstName = firstName;
//   this.birthYear = birthYear;
// };

// Person.prototype.calcAge = function () {
//   const curYear = new Date().getFullYear();
//   console.log(curYear - this.birthYear);
//   return curYear - this.birthYear;
// };

// const Student = function (firstName, birthYear, course) {
//   Person.call(this, firstName, birthYear);
//   this.course = course;
// };

// Student.prototype = Object.create(Person.prototype);

// Student.prototype.introduce = function () {
//   console.log(`My name is ${this.firstName} and I study in ${this.course}.`);
// };

// const mike = new Student('Mike', 2020, 'Computer Science');

// mike.introduce();
// mike.calcAge();

// console.log(mike.__proto__);
// console.log(mike.__proto__.__proto__);

// console.log(mike instanceof Student);
// console.log(mike instanceof Person);
// console.log(mike instanceof Object);

// Student.prototype.constructor = Student;

// console.log(Student.prototype.constructor);
// console.dir(Student.prototype.constructor);

// class PersonCl {
//   constructor(fullName, birthYear) {
//     this.fullName = fullName;
//     this.birthYear = birthYear;
//   }

//   calcAge() {
//     return 2037 - this.birthYear;
//   }

//   greet() {
//     return `Hey ${this.fullName}`;
//   }

//   get age() {
//     return 2037 - this.birthYear;
//   }

//   set fullName(name) {
//     if (name.includes(` `)) this._fullName = name;
//     else alert(`${name} is not a full name!`);
//   }

//   get fullName() {
//     return this._fullName;
//   }

//   static hey() {
//     console.log(`Hey there ehe`);
//   }
// }
// class StudentCl extends PersonCl {
//   constructor(fullName, birthYear, course) {
//     // Always needs to happen first!
//     super(fullName, birthYear);
//     this.course = course;
//   }

//   introduce() {
//     return `My name is ${this.fullName} and I study ${this.course}.`;
//   }

//   calcAge() {
//     return `I'm ${2037 - this.birthYear}, but as a student I feel more like ${
//       2037 - this.birthYear + 10
//     }.`;
//   }
// }

// const martha = new StudentCl('Martha Jones', 2012, `Computer Science`);

// const PersonProto = {
//   calcAge() {
//     return 2037 - this.birthYear;
//   },

//   init(firstName, birthYear) {
//     this.firstName = firstName;
//     this.birthYear = birthYear;
//   },
// };

// const steven = Object.create(PersonProto);

// const StudentProto = Object.create(PersonProto);
// StudentProto.init = function (firstName, birthYear, course) {
//   PersonProto.init.call(this, firstName, birthYear);
//   this.course = course;
// };

// StudentProto.introduce = function () {
//   return `My name is ${this.fullName} and I study ${this.course}.`;
// };

// const jay = Object.create(StudentProto);
// jay.init(`Jay`, 2010, 'Computer Science');
// jay.introduce();
// jay.calcAge();

// 1. Public Fields
// 2. Private Fields
// 3. Public Methods
// 4. Private Methods
class Account {
  // Public Fields
  locale = navigator.language;

  // Private Fields
  #movements = [];
  #pin;

  constructor(owner, currency, pin) {
    this.owner = owner;
    this.currency = currency;
    this.#pin = pin;
    // Protected
    // this._movements = [];
    // this.locale = navigator.language;

    console.log(`Thanks for opening an account, ${owner}`);
  }

  // Public Interface
  getMovements() {
    return this.#movements;
  }

  deposit(val) {
    this.#movements.push(val);
    return this;
  }

  withdraw(val) {
    this.deposit(-val);
    return this;
  }

  requestLoan(val) {
    if (this._approveLoan(val)) {
      this.deposit(val);
      console.log(`Loan Approved`);
      return this;
    }
  }

  static helper() {
    console.log('Ehe ');
  }

  // Private Methods
  _approveLoan(val) {
    return true;
  }
}

const acc1 = new Account('Alzy', 'EUR', 1111);
console.log(acc1);

// acc1.#movements.push(230);
// acc1.#movements.push(-129);

acc1.deposit(230);
acc1.withdraw(230);
acc1.requestLoan(1000);
// acc1._approveLoan(1000);

console.log(acc1.getMovements());

console.log(acc1);

Account.helper();

acc1.deposit(300).deposit(500).withdraw(35).requestLoan(25000).withdraw(4000);

console.log(acc1.getMovements());
